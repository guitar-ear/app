import 'package:flutter/material.dart';

import 'package:guitar_ear/library.dart';
import 'package:guitar_ear/loginView.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:shared_preferences/shared_preferences.dart';

import './webview.dart';
import './library.dart';
import './globals.dart';

import './loginView.dart';
import './home.dart';
import './loginMethods.dart';

class Login extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _LoginState();
  }
}

class _LoginState extends State<Login> {

  void initState() {
    super.initState();
    _loadToken();
  }

  _loadToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print(prefs.getString('access_token'));

    if(aToken == null) {
      print("non esiste");
    } else {
      print("esiste");
      var data = await GetData(prefs.getString('access_token'));
      print("adaad $userData");
      setState(() {
        userData = data;
        isLoggedIn = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    /*
    return Scaffold(
        body: Center(
          child: !isLoggedIn ? Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              RawMaterialButton(
                onPressed: () async{
                  var token = await LoginResponse();
                  var data = await GetData(token['access_token']);

                  // obtain shared preferences
                  final prefs = await SharedPreferences.getInstance();

                  prefs.setString('access_token', token['access_token'].toString());
                  prefs.setString('refresh_token', token['refresh_token'].toString());

                  print(data['id']);

                  setState(() {
                    aToken = token['access_token'];
                    rToken = token['refresh_token'];
                    userData = data;
                    isLoggedIn = true;
                    refresh = true;
                  });
                },
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(18.0),
                  side: BorderSide(color: Colors.blue),

                ),
                fillColor: Colors.blue,
                child: Text("login",
                  style: TextStyle(
                      fontFamily: 'Open Sans',
                      color: Colors.white,
                      fontSize: 20
                  ),
                ),
              ) ,
              RawMaterialButton (
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text("indietro")
              )
            ],
          ) : LoginView(),
        )
    ); */

    if(isLoggedIn) {
      return LoginView();
    } else {
      return LoginMethods();
    }

  }

}


LoginResponse() async {
  var Launcher = UrlLauncher();
  var token;
  var risp;

  token = await Launcher.Launch();
  var Obtain = ObtainToken(token);

  do {
    risp = await GetRequest(Obtain);
  }while(risp['access_token'] == null);

  return risp;
}

GetRequest(Obtain) async {

  var risp = await Future.delayed(Duration(milliseconds: 2500), () {
    print('1');
    return Obtain.makeGetRequest();
  });

  return risp;
}

GetData(token) async{
  var url = "https://guitarear.live/user-info";
  var data = await ObtainToken(token).makeGetData(url);

  return data;
}


/*

Status() async{
  var Obtain = ObtainToken();

  var risposta = Obtain.getRisposta();
  while( risposta == null) {
    Status();
  } return risposta;
}*/


/*

https://guitarear.live/token-login?token-app=

ogni poco faccio una richiesta
guitear.live/token/emit/{token}
  se non ha loggato
    cod errore -> 401
  se non esiste piu' (sono passati 5 min)
    cod errore -> 404
  se ha loggato
    cod _> 200  e un json {access_token(chiave)
                           refresh_token) ogni venti min mando il refresh (guitarear.live/token/refresh) per avere un altro access token, che serve per le richieste api
                           per il refresh nell'header della richiesta -> Barrier {refresh_token}
  per i dati dell'uytente (guitarear.live/user-info
  API
  Barrier (access_token)

  change propic:
  /avatar-set  -> richiesta modalita' put

  Song api:
  /api/ai/v0/song



  cartella apis guitarear.live/api/v0/
 */