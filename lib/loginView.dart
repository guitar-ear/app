import 'package:flutter/material.dart';
import 'package:guitar_ear/globals.dart';
import 'package:guitar_ear/main.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';

import './login.dart';
import './home.dart';
import './globals.dart';
import './listView.dart';

class LoginView extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    print(userData['first_name'].toString());
    // TODO: implement createState
    return _LoginViewState();
  }
}

class _LoginViewState extends State<LoginView> {
  List<List<String>> canzoni = [];

  @override
  void initState() {
    // TODO: implement initState
    if(refresh) {
      setState(() {
        print("SETSTATE LOGINVIEW");
        refresh = false;
      });
    }
    setCanzoni();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    // TODO: implement build
    return Scaffold (
      body: Container(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Center(
              child: Text("Le tue canzoni"),
            ),
            Flexible(
                child: ListViewBuilders(canzoni))
          ],
        ),
      ),
    );
  }

  setCanzoni() async{
    var prefs = await SharedPreferences.getInstance();
    int i = 0;

    while(prefs.getStringList(i.toString()) != null) {
      canzoni.add(prefs.getStringList(i.toString()));
      print(prefs.getStringList(i.toString()));
      i++;
    }

    return;
  }
}

