import 'package:flutter/material.dart';
import 'package:guitar_ear/globals.dart';
import './scan.dart';

import 'scan.dart';

class Button extends StatefulWidget {
  var scritta;
  var from;

  Button (this.scritta, this.from);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ButtonState();
  }
}

class _ButtonState extends State<Button> with TickerProviderStateMixin {

  AnimationController _breathingController;
  var _breathe = 0.0;


  @override
void initState() {
  // TODO: implement initState
  super.initState();
  _breathingController = AnimationController(vsync: this, duration: new Duration(seconds: 2));
  _breathingController.addStatusListener((status){
    if(status == AnimationStatus.completed) {
      _breathingController.reverse();
    } else if (status == AnimationStatus.dismissed) {
      _breathingController.forward();
    }
  });
  _breathingController.addListener((){
    setState(() {
      _breathe = _breathingController.value;
    });
  });
  _breathingController.forward();


}

void dispose() {
  _breathingController.dispose();
  super.dispose();
}

@override
Widget build(BuildContext context) {
  final size = 200.0 - 20.0 * _breathe;
  // TODO: implement build
  return GestureDetector(
          onTap: () {
            if(widget.from == "home") {
              if(isLoggedIn) {
                Navigator.push(context, MaterialPageRoute(builder: (context) =>Scan()));
              } else {
                Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text('devi loggarti per iniziare ad usare GuitarEar!'),
                ));
              }

            }
          },
          child: Container(
            //color: Color(0xff00ccff),
              width: size,
              height: size,
              margin: EdgeInsets.all(10.00),
              decoration: BoxDecoration(
                shape: BoxShape.circle,

                boxShadow: [
                  BoxShadow(
                    color: Color(0xff00ccff).withOpacity(0.3),
                    spreadRadius: 2,
                    blurRadius: 7,
                    //offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Material(
                    borderRadius: BorderRadius.circular(size),
                    color: Colors.white,
                    child: Center(
                      child: Text(widget.scritta,
                        style: TextStyle(
                            //fontFamily: 'Open Sans',
                            color: Colors.grey[800],
                            fontSize: 20 + (size*0.1)
                        ),
                      ),
                    )

                ),

          ),
        );
}

}