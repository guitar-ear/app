import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ListViewBuilder extends StatelessWidget {
  List <List<String>> canzoni = [];
  var titoli;
  var artisti;

  ListViewBuilder(l) {
    canzoni = l;
    setColumn();
  }

  setColumn() {
    titoli = canzoni.map<String>((row) => row[0]).toList(growable: false);
    artisti = canzoni.map<String>((row) => row[1]).toList(growable: false);

  }

  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.all(6.0),

          child: Container(
            //height: 50,
            //color: Colors.blue,
            child: Card(
              child: Column(
                children: <Widget>[
                  ListTile(
                    title: Text(titoli[index]),
                    subtitle: Text(artisti[index]),
                    trailing: GestureDetector(
                      onTapDown: (TapDownDetails details) {
                        menuPopup(context, details.globalPosition, index);
                      },
                      child: Icon(Icons.more_vert, color: Color(0xff00ccff),),
                    ),

                  ),
                ],
              )
            ),
          ),
        );
      },
      itemCount: canzoni.length,
    );
  }

  menuPopup(context, Offset offset, index) async{
    double left = offset.dx;
    double top = offset.dy;
    await showMenu(
        context: context,
        position: RelativeRect.fromLTRB(left, top, 0, 0),
        items: [
          PopupMenuItem(
            child: IconButton(
              onPressed: await deleteSong(index),
              icon: Icon(Icons.delete, color: Colors.red,),
            ),
          ),
          PopupMenuItem(
            child: IconButton(
              //onPressed: deleteSong,
              icon: Icon(Icons.share, color: Colors.grey,),
            ),
          ),
        ]
    );
    /*

    return PopupMenuButton<String> (
      onSelected: choiceAction,
      itemBuilder: (BuildContext context) {
        return Constants.choices.map((String choice) {
          return PopupMenuItem<String>(
            value: choice,
            child: Text(choice),
          );
        }).toList();
      },
    ); */
  }
  deleteSong(index) async {
    final prefs = await SharedPreferences.getInstance();
    //print(index);

    prefs.remove(index.toString());


  }
}

class ListViewBuilders extends StatefulWidget {

  List <List<String>> canzoni = [];
  var titoli;
  var artisti;

  ListViewBuilders(l) {
    canzoni = l;
    setColumn();
  }

  setColumn() {
    titoli = canzoni.map<String>((row) => row[0]).toList(growable: false);
    artisti = canzoni.map<String>((row) => row[1]).toList(growable: false);

  }

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ListViewBuilderState();
  }
}

class _ListViewBuilderState extends State<ListViewBuilders> {

  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.all(6.0),

          child: Container(
            //height: 50,
            //color: Colors.blue,
            child: Card(
                child: Column(
                  children: <Widget>[
                    ListTile(
                      title: Text(widget.titoli[index]),
                      subtitle: Text(widget.artisti[index]),
                      trailing: GestureDetector(
                        onTapDown: (TapDownDetails details) {
                          menuPopup(context, details.globalPosition, index);
                        },
                        child: Icon(Icons.more_vert, color: Color(0xff00ccff),),
                      ),

                    ),
                  ],
                )
            ),
          ),
        );
      },
      itemCount: widget.canzoni.length,
    );
  }

  menuPopup(context, Offset offset, index) async{
    double left = offset.dx;
    double top = offset.dy;
    await showMenu(
        context: context,
        position: RelativeRect.fromLTRB(left, top, 0, 0),
        items: [
          PopupMenuItem(
            child: IconButton(
              //onPressed: await deleteSong(index),
              icon: Icon(Icons.delete, color: Colors.red,),
            ),
          ),
          PopupMenuItem(
            child: IconButton(
              //onPressed: deleteSong,
              icon: Icon(Icons.share, color: Colors.grey,),
            ),
          ),
        ]
    );
    /*

    return PopupMenuButton<String> (
      onSelected: choiceAction,
      itemBuilder: (BuildContext context) {
        return Constants.choices.map((String choice) {
          return PopupMenuItem<String>(
            value: choice,
            child: Text(choice),
          );
        }).toList();
      },
    ); */
  }
  deleteSong(index) async {
    final prefs = await SharedPreferences.getInstance();

    prefs.remove(index.toString());
    widget.canzoni.removeAt(index);
    setState(() {

    });

  }
}


