import 'dart:io' as io;
import 'package:flutter/material.dart';
import 'package:flutter_audio_recorder/flutter_audio_recorder.dart';
import 'package:dio/dio.dart';
import 'package:path_provider/path_provider.dart';
import 'package:file/file.dart';
import 'package:file/local.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';

import './globals.dart';
import './songData.dart';


class Scan extends StatefulWidget {
  final LocalFileSystem localFileSystem;

  Scan({localFileSystem})
      : this.localFileSystem = localFileSystem ?? LocalFileSystem();

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ScanState();
  }
}

class _ScanState extends State<Scan> with TickerProviderStateMixin{
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  SendSong Send;
  FlutterAudioRecorder _recorder;
  Recording _current;
  RecordingStatus _currentStatus = RecordingStatus.Unset;
  bool stop = false;
  var watch = new Stopwatch();
  String durata;
  bool downloading = false;
  var progressString = "";

  TabController controller;
  AnimationController _animationController;
  Animation _colorTween;

  @override
  void initState() {
    // TODO: implement initState
    _init();
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    _colorTween = ColorTween(begin: Color(0xff00ccff), end: Colors.redAccent)
        .animate(_animationController);


    super.initState();
  }

  void dispose() {
    super.dispose();
    _animationController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
        key: _scaffoldKey,
        body: Center(
          child: Container(
            width: 200,

            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(bottom: 10),
                      child: Text( durata != null ? "$durata" : "00:00",
                        style: TextStyle(
                            fontSize: 60
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  //crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Expanded(
                      //padding: EdgeInsets.all(100),
                      //color: Colors.yellowAccent,
                      child: Container(
                        height: 130,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            boxShadow: [
                              BoxShadow(
                                color: _colorTween.value.withOpacity(0.3),
                                spreadRadius: 2,
                                blurRadius: 7,
                                offset: Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                        child: AnimatedBuilder(
                          animation: _colorTween,
                          builder: (context, child) => MaterialButton(
                            minWidth: double.infinity,
                            onPressed: () {
                              switch (_currentStatus){
                                case RecordingStatus.Initialized:
                                  {
                                    Send = new SendSong();
                                    startRecording();

                                    break;
                                  }
                                case RecordingStatus.Recording:
                                  {

                                    SongData form = new SongData();
                                    stopStopwatch();
                                    setState(() {
                                      stop = true;
                                    });
                                    form.createDialog(context);
                                    //downloadFile();
                                    break;
                                  }
                                case RecordingStatus.Paused:
                                  {
                                    //_resume();
                                    break;
                                  }
                                case RecordingStatus.Stopped:
                                  {
                                    //_init();
                                    break;
                                  }
                                default:
                                  break;
                              }
                            },
                            child: _buildText(_currentStatus),
                            color: _colorTween.value,
                            //color: Color(0xff00ccff),
                            shape: CircleBorder(),
                            elevation: 0,
                          )
                        )
                      ),
                    )
                  ],
                ),

                //Button("?", "scan"),
                RawMaterialButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text("torna indietro "),
                )
              ],
            ),
          )
        ),
    );
  }

  startRecording() async{
    await Send.obtainSongId();

    errorControl();
    _animationController.forward();
    startStopwatch();
    backgroundDivision();

  }

  errorControl() {
    if(Send.error != null) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(Send.error),
      ));
      setState(() {
        print("ERRORE IN ERROR CONTROL");
        Send.error = null;
      });
    }
  }

  Future _init() async{
    print("INIT");
    try {
      if (await FlutterAudioRecorder.hasPermissions) {
        String customPath = '/flutter_audio_recorder_';
        io.Directory appDocDirectory;
//        io.Directory appDocDirectory = await getApplicationDocumentsDirectory();
        if (io.Platform.isIOS) {
          appDocDirectory = await getApplicationDocumentsDirectory();
        } else {
          appDocDirectory = await getExternalStorageDirectory();
        }

        // can add extension like ".mp4" ".wav" ".m4a" ".aac"
        customPath = appDocDirectory.path +
            customPath +
            DateTime.now().millisecondsSinceEpoch.toString();

        // .wav <---> AudioFormat.WAV
        // .mp4 .m4a .aac <---> AudioFormat.AAC
        // AudioFormat is optional, if given value, will overwrite path extension when there is conflicts.
        _recorder =
            FlutterAudioRecorder(customPath, audioFormat: AudioFormat.WAV);

        await _recorder.initialized;
        // after initialization
        var current = await _recorder.current(channel: 0);
        print(current);
        // should be "Initialized", if all working fine
        setState(() {
          _current = current;
          _currentStatus = current.status;
          print(_currentStatus);
        });
      } else {
        Scaffold.of(context).showSnackBar(
            new SnackBar(content: new Text("You must accept permissions")));
      }
    } catch (e) {
      print(e);
    }

  }
  _start() async {
    print("START");
    try {
      await _recorder.start();
      var recording = await _recorder.current(channel: 0);
      setState(() {
        _current = recording;
      });

      const tick = const Duration(milliseconds: 50);
      new Timer.periodic(tick, (Timer t) async {
        if (_currentStatus == RecordingStatus.Stopped) {
          t.cancel();
        }

        var current = await _recorder.current(channel: 0);
        // print(current.status);
        setState(() {
          _current = current;
          _currentStatus = _current.status;
        });


      });
    } catch (e) {
      print(e);
    }
  }

  backgroundDivision() async{
    while(!stop) {
        await _init();
        await _start();
        await Future.delayed(Duration(seconds: 4), () async{
          var path = await _stop();
          Send.send(path);
          errorControl();
        });
      print("andando");
    }

    print("fine");
    await Send.process();
    errorControl();
    await downloadFile();
  }

  _stop() async {
    print("STOP");
    var result = await _recorder.stop();
    print("Stop recording: ${result.path}");
    print("Stop recording: ${result.duration}");
    File file = widget.localFileSystem.file(result.path);
    print(file.length());

    print("File length: ${await file.length()}");
    //Send.client.close();
    //Send.send(result.path);
    setState(() {
      _current = result;
      _currentStatus = _current.status;
    });
    return result.path;
  }

  Widget _buildText(RecordingStatus status) {
    var text = "";
    var icona;
    switch (_currentStatus) {
      case RecordingStatus.Initialized:
        {
          icona = Icons.mic;
          text = 'Start';
          break;
        }
      case RecordingStatus.Recording:
        {
          icona = Icons.mic_off;
          text = 'Stop';
          break;
        }
      case RecordingStatus.Paused:
        {
          icona = Icons.mic_off;
          text = 'Resume';
          break;
        }
      case RecordingStatus.Stopped:
        {
          icona = Icons.mic_off;
          text = 'Init';
          break;
        }
      default:
        break;
    }
    //return Text(text, style: TextStyle(color: Colors.white));
    return Icon(icona, color: Colors.white);
  }
  //STOPWATCH
  startTimer() {
    Timer(Duration(seconds: 1), keepRunning);
  }

  keepRunning() {
    if(watch.isRunning) {
      startTimer();
    }
    setState(() {
      durata = (watch.elapsed.inMinutes%60).toString().padLeft(2, "0") + ":" + (watch.elapsed.inSeconds%60).toString().padLeft(2, "0");
    });
  }

  startStopwatch() {

    watch.start();
    startTimer();

  }

  stopStopwatch() {
    watch.stop();
  }

  Future<void> downloadFile() async {
    var dir = await getApplicationDocumentsDirectory();
    var songId = Send.songId.toString();
    //var urlMidiFile =  Send.urlAI + songId;
    //var path = "${dir.path}/$songId/$songId.mid";
    var urlMidiFile =  Send.urlAI + "$songId";
    //var path = "${dir.path}/$songId.mid";

    var tempDir = await getTemporaryDirectory();
    var path = "${dir.path}/$songId.mid";
    //String fullPath = tempDir.path + "/adam.mid";
    String fullPath = "${tempDir.path}/$songId";
    print('full path ${fullPath}');

    Dio dio = Dio();



    try {
      Response response = await dio.get(
        urlMidiFile,
        //Received data with List<int>
        options: Options(
            headers: {"Authorization": "Bearer $aToken"},
            responseType: ResponseType.bytes,
            followRedirects: false,
            validateStatus: (status) {
              print(status);
              return status < 500;
            }),
      );

      print(response.headers);
      print(urlMidiFile);
      io.File file = io.File(path);
      var raf = file.openSync(mode: FileMode.write);
      // response.data is List<int> type
      raf.writeFromSync(response.data);
      await raf.close();
    } catch (e) {
      print(e);
    }

  }

}


class SendSong {
  var songId;
  var inizio;
  var urlSong = "https://guitarear.live/api/ai/v0/song/";
  var urlAI = "https://guitarear.live/api/ai/v0/song/process/";
  var error;
  io.HttpClient client;
  List <String> fragments = [];

  /*
  SendSong() {
    client = new io.HttpClient();
    error = null;
    obtainSongId();
  }*/

  obtainSongId() async{
    client = new io.HttpClient();
    var uri = Uri.parse(urlSong);
    print(aToken);
    print('OBTAINID');


    io.HttpClientRequest request = await client.getUrl(uri);
    request.headers.set("Authorization", "Bearer $aToken");

    io.HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();

    if(!statusCodeCheck(response)) return;

    try {
      Map<String, dynamic> risposta = jsonDecode(reply);
      print("OBTAIND successo");
      print(risposta);
      if(risposta['id'] != null)
        this.songId = risposta['id'];
      else
        throw Exception;
    } on Exception catch(e) {
      error = "si e' verificato un problema con il server";
      //throw(e);
    }

  }

  send(path) async{
    fragments.add(path);
    var urlFile = urlSong + '$songId';
    print(urlFile);

    Map<String, String> headers = { "authorization": "Bearer $aToken"};

    var requestFile = http.MultipartRequest('PUT', Uri.parse(urlFile));
    requestFile.headers.addAll(headers);
    requestFile.files.add(await http.MultipartFile.fromPath(
      'file', path,
    ));
    var responseFile = await requestFile.send();

    if(!statusCodeCheck(responseFile)) return;

  }

  process() async{
    var i = 0;
    var urlFragments = urlAI + '$songId';

    Map<String, String> headers = { "authorization": "Bearer $aToken"};
    print("PROCESS");

    while(i < fragments.length ) {
      await Future.delayed(Duration(milliseconds: 1000), () async{
        print("PROCESS $i");
        var requestFile = http.MultipartRequest('POST', Uri.parse(urlFragments));
        requestFile.headers.addAll(headers);
        requestFile.files.add(await http.MultipartFile.fromPath(
          'file', fragments[i],
        ));
        var response = await requestFile.send();
        if(!statusCodeCheck(response)) return;
        i++;
      });
    }

    /*
    await Future.delayed(Duration(milliseconds: 4000), () async{
      var requestFile = http.MultipartRequest('GET', Uri.parse(urlFragments));
      requestFile.headers.addAll(headers);

      print(requestFile);
    });*/

  }

  statusCodeCheck(status) {
    if(status.statusCode > 202) {
      error = "si e' verificato un problema con il server";
      return false;
    } else {
      return true;
    }
  }


}



///process/id
