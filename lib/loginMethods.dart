import 'package:flutter/material.dart';

import 'package:guitar_ear/library.dart';
import 'package:guitar_ear/loginView.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';

import './webview.dart';
import './library.dart';
import './globals.dart';

import './loginView.dart';
import './home.dart';

class LoginMethods extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _LoginMethodStates();
  }
}

class _LoginMethodStates extends State<LoginMethods> {

  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white,
        body: Container(
          child: Column(

            children: <Widget>[
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("assets/images/prova4.png"),
                        //fit: BoxFit.fitHeight,
                        alignment: Alignment.topCenter
                      )
                  ),
                ),

              ),
              Container(
                margin: const EdgeInsets.only(bottom: 60),

                height: 50,
                width: 100,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.lightBlueAccent.withOpacity(0.3),
                      spreadRadius: 2,
                      blurRadius: 7,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                child: RawMaterialButton (
                  elevation: 0.0,
                  onPressed: () async{
                    var token = await LoginResponse();
                    var data = await GetData(token['access_token']);

                    // obtain shared preferences
                    final prefs = await SharedPreferences.getInstance();

                    prefs.setString('access_token', token['access_token'].toString());
                    prefs.setString('refresh_token', token['refresh_token'].toString());

                    print(data['id']);

                    setState(() {
                      aToken = token['access_token'];
                      rToken = token['refresh_token'];
                      userData = data;
                      isLoggedIn = true;
                      Phoenix.rebirth(context);
                    });
                  },
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(25.0),

                  ),
                  fillColor: Color(0xff00ccff),
                  child: Text("Login",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),
                ),
              ),

            ],
          )
        )
    );
  }

}


LoginResponse() async {
  var Launcher = UrlLauncher();
  var token;
  var risp;

  token = await Launcher.Launch();
  var Obtain = ObtainToken(token);

  do {
    risp = await GetRequest(Obtain);
  }while(risp['access_token'] == null);

  return risp;
}

GetRequest(Obtain) async {

  var risp = await Future.delayed(Duration(milliseconds: 2500), () {
    print('1');
    return Obtain.makeGetRequest();
  });

  return risp;
}

GetData(token) async{
  var url = "https://guitarear.live/user-info";
  var data = await ObtainToken(token).makeGetData(url);

  return data;
}


/*

Status() async{
  var Obtain = ObtainToken();

  var risposta = Obtain.getRisposta();
  while( risposta == null) {
    Status();
  } return risposta;
}*/


/*

https://guitarear.live/token-login?token-app=

ogni poco faccio una richiesta
guitear.live/token/emit/{token}
  se non ha loggato
    cod errore -> 401
  se non esiste piu' (sono passati 5 min)
    cod errore -> 404
  se ha loggato
    cod _> 200  e un json {access_token(chiave)
                           refresh_token) ogni venti min mando il refresh (guitarear.live/token/refresh) per avere un altro access token, che serve per le richieste api
                           per il refresh nell'header della richiesta -> Barrier {refresh_token}
  per i dati dell'uytente (guitarear.live/user-info
  API
  Barrier (access_token)

  change propic:
  /avatar-set  -> richiesta modalita' put

  Song api:
  /api/ai/v0/song



  cartella apis guitarear.live/api/v0/
 */