import 'package:flutter/material.dart';
import 'package:async/async.dart';
import 'package:shared_preferences/shared_preferences.dart';

import './tabView.dart';

class SongData{
  final formKey = GlobalKey<FormState>();

  List <String> dati = [];
  var titolo;
  var prefs;

  _submit(context) async{
    getPrefs();
    formKey.currentState.save();
    final prefs = await SharedPreferences.getInstance();
    var key = await getKey();
    print(key);
    prefs.setStringList(key.toString(), dati);
    prefs.setInt("numCanzoni", key+1);

    Navigator.push(context, MaterialPageRoute(builder: (context) =>TabView()));
  }

  getPrefs() async{
    prefs = await SharedPreferences.getInstance();
  }

  getKey() async{
    var i = 0;
    if(prefs.getStringList('0') == null) {
      i = 0;
    } else {
      while(prefs.getStringList(i.toString()) != null) {
        i++;
      }
    }

    return i;
  }

  createDialog(BuildContext context) {
    return showDialog(context: context, builder: (context){
      return AlertDialog(
        title: Text("inserisci i dati!"),
        content: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Form(
                key: formKey,
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      decoration: InputDecoration(
                        labelText: "Titolo",
                        //border: OutlineInputBorder(),
                      ),
                      onSaved: (input){dati.add(input); titolo = input;},
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                        labelText: "Artista",
                        //border: OutlineInputBorder(),
                      ),
                      onSaved: (input) => dati.add(input),
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.only(top: 10),
                            height: 70,
                            child: (
                                MaterialButton(
                                    child: Text('salva',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 20
                                      ),
                                    ),
                                    onPressed: () {
                                      _submit(context);
                                    },
                                    color: Color(0xff00ccff)
                                )
                            ),
                          )
                        )
                        ,
                      ],
                    )
                  ],
                ),
              ),


            ],
          ),
        ),
      );
    });
  }

}
