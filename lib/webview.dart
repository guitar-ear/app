import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:math';
import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:http/http.dart' as http;


/*
class WebViewClass extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _WebViewClassState();
  }
}*/

/*
class _WebViewClassState extends State<WebViewClass> {*/


class UrlLauncher {
  Launch() async{
    String url = "https://guitarear.live/token-login?token-app=";
    String token = Generator.CreateCryptoRandomString();
    print(url+token);
    //return _launchURL(url, token);
    if (await canLaunch(url+token)) {
      await launch(url+token);

    } else {
      throw 'Could not launch $url';
    }

    return token;
  }

}


class Generator {
  static final Random _random = Random.secure();

  static String CreateCryptoRandomString([int length = 128]) {
    var values = List<int>.generate(length, (i) => _random.nextInt(256));

    var bytes = utf8.encode(base64Url.encode(values));

    return sha512.convert(bytes).toString();
  }
}


class ObtainToken {
  var url = "https://guitarear.live/token/emit/";
  HttpClient client = new HttpClient();

  var Status;
  String token;
  Map<String, dynamic> risposta;
  String refreshToken;

  ObtainToken(this.token);

  String getStatus() {
    return Status;
  }

  Map<String, dynamic> getAccessToken() {
    return risposta;
  }


  Future <Map<String, dynamic>> makeGetRequest() async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);

    HttpClientRequest request = await client.getUrl(Uri.parse(url+token));

    HttpClientResponse response = await request.close();

    String reply = await response.transform(utf8.decoder).join();

    Map<String, dynamic> risposta = jsonDecode(reply);

    var success = risposta['code'];
    print("GET REQUEST $success");

    client.close();
    return risposta;

  }

  Future <Map<String, dynamic>> makeGetData(url) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);

    HttpClientRequest request = await client.getUrl(Uri.parse(url));
    request.headers.set("Authorization", "Bearer $token");

    HttpClientResponse response = await request.close();

    String reply = await response.transform(utf8.decoder).join();

    Map<String, dynamic> risposta = jsonDecode(reply);

    var success = risposta['code'];
    print(success);
    client.close();

    return risposta;

  }


}


