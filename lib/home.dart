import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io';
import 'dart:convert';

import './login.dart';
import './button.dart';
import './globals.dart';
import './profile.dart';

class HomeScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _HomeScreenState();
  }
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin{
  bool _show = true;

  TabController controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller = new TabController(length: 3, vsync: this);
    controller.addListener(_handleTabSelection);
    controller.index = 1;
    loginControl();
  }

  void loginControl() async{
    print("loginCVontrol");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print(prefs.getString('access_token'));
    print(prefs.getString('refresh_token'));

    if(!prefs.containsKey('refresh_token')) {
      aToken = null;
      rToken = null;
      isLoggedIn = false;
      print('non loggato');
      //userData = null;
    } else {
      renewAToken();
      print("loggato");
      userData = await GetData(prefs.getString('access_token'));
      isLoggedIn = true;
      aToken = prefs.getString('access_token');
      rToken = prefs.getString('refresh_token');
      //userData = data;

    }
  }


  void _handleTabSelection() {
    setState(() {
    });
  }

  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
        appBar: _show ? AppBar(
          title: new Center(

              child: Text('GuitarEar',
                style: TextStyle(
                    fontFamily: 'Open Sans',
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                ),
              )
          ),
          backgroundColor: Colors.white,
          elevation: 1.0,
        ) : PreferredSize(preferredSize: Size(0.0, 0.0),child: Container(),) ,
        bottomNavigationBar: _show ? new Material(

            color: Colors.white,
            child: new TabBar(

              indicatorColor: Colors.transparent,
              controller: controller,
              tabs: <Tab> [
                new Tab(icon: new Icon(Icons.account_circle,
                    color: controller.index == 0
                        ? Colors.blue
                        : Colors.black)),
                new Tab(icon: new Icon(Icons.music_note,
                    color: controller.index == 1
                        ? Colors.blue
                        : Colors.black)),
                new Tab(icon: new Icon(Icons.library_music,
                    color: controller.index == 2
                        ? Colors.blue
                        : Colors.black)),
              ],
            )
        ) : PreferredSize(preferredSize: Size(0.0, 0.0),child: Container(),) ,
        body: new TabBarView(
            controller: controller,

            children: <Widget> [
              Profile(),
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Button("inizia", "home"),

                  ],
                ),
              ),
              Login()
            ]
        )
    );
  }
}

renewAToken() async{

  while(true)  {
    await Future.delayed(Duration(minutes: 1), () {
      print("REFRESHING");
      getNewToken();
    });
  }


}

Future <String> getNewToken() async{
  HttpClient client = new HttpClient();
  client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);

  var url = "https://guitarear.live/token/refresh";
//  var response = await http.put(url, body: file, headers: {"Authorization":"Bearer $tokens['access_token']"});
//  print(response);

  HttpClientRequest request = await client.postUrl(Uri.parse(url));
  request.headers.set("Authorization", "Bearer $rToken");

  HttpClientResponse response = await request.close();

  String reply = await response.transform(utf8.decoder).join();

  Map<String, dynamic> risposta = jsonDecode(reply);
  if (response.statusCode == 200) {
    aToken = risposta['access_token'];
    setLocal();
  }
  print(risposta);
  return risposta['access_token'];
}

setLocal() async{
  SharedPreferences prefs = await SharedPreferences.getInstance();

  prefs.setString('access_token', aToken.toString());
}