import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'globals.dart';
import 'login.dart';

class Profile extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ProfileState();
  }
}

class _ProfileState extends State<Profile> {
  var numCanzoni;
  @override
  void initState() {
    // TODO: implement initState
    getNumCanzoni().then((n) {
      numCanzoni = n;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    // TODO: implement build
    return Center(
      child: isLoggedIn ? Align (
        alignment: Alignment(0.0, -0.8),
        child: Container(
          //color: Colors.red,
         // padding: const EdgeInsets.only(top: 20.0),
          //height: 100%,
          constraints: BoxConstraints.expand(),
            child: Column(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  color: Colors.lightBlueAccent,
                  child: Material(
                    color: Colors.white,
                    elevation: 5,
                    child: Column(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top: 30),
                          child: Material(
                            elevation: 10,
                            shape: CircleBorder(),
                            child: ClipOval(
                              child: Image.network(
                                proPic,
                                height: 120.0,
                                width: 120.0,
                              ),
                            ),
                          ),
                        ),

                        Container (
                          margin: const EdgeInsets.only(top: 10.0),
                          child: Text(userData['first_name'].toString().inCaps+" "+userData['last_name'].toString().inCaps,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18
                            ),
                          ),
                        ),

                        Container (
                          margin: const EdgeInsets.only(bottom: 30.0),
                          child: Text("@"+userData['username'].toString(),
                            style: TextStyle(
                                fontSize: 16
                            ),
                          ),
                        ),

                        Container (
                          padding: EdgeInsets.only(bottom: 20),
                          child: Text("Canzoni: $numCanzoni",
                            style: TextStyle(
                              fontWeight: FontWeight.bold
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),

                //SECONDA META

                Expanded (
                  child : Container(
                    //height: 500,
                    color: Color(0xfff2fbfc),
                    width: double.infinity,
                    //color: Colors.blue,
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: const EdgeInsets.only(top: 40),

                          height: 50,
                          width: 100,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25.0),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.lightBlueAccent.withOpacity(0.3),
                                spreadRadius: 2,
                                blurRadius: 7,
                                offset: Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                          child: RawMaterialButton (
                            elevation: 0.0,
                            onPressed: () async{
                              await createDialog(context);
                              Phoenix.rebirth(context);
                            },
                            shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(25.0),

                            ),
                            fillColor: Color(0xff00ccff),
                            child: Text("Logout",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                              ),
                            ),
                          ),
                        ),


                      ],
                    )

                  )

                ),


              ], 
            ),
        ),
      ): Login()
    );
  }
}


Logout() async{


  final prefs = await SharedPreferences.getInstance();

  prefs.remove('access_token');
  prefs.remove('refresh_token');
  isLoggedIn = false;

}

createDialog(BuildContext context) {
  return showDialog(context: context, builder: (context){
    return AlertDialog(
      title: Text("effettuare il logout?"),
      actions: <Widget>[
        RawMaterialButton(
            child: Text('Si',
              style: TextStyle(
                color: Colors.lightBlue,
                fontWeight: FontWeight.bold
              ),
            ),
            onPressed: () {
              Logout();
              Navigator.of(context).pop();
            },
            //fillColor: Colors.lightBlue
        )
      ],
    );;
  });
}

getNumCanzoni() async{
  var prefs = await SharedPreferences.getInstance();
  if(prefs.getInt("numCanzoni") == null) {
    return '0';
  } else {
    return prefs.getInt("numCanzoni").toString();
  }
}