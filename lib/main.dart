import 'dart:io';

import 'package:flutter/material.dart';
import 'package:async/async.dart';

import 'package:flutter_phoenix/flutter_phoenix.dart';

import "./home.dart";
import './globals.dart';
import './login.dart';

class MyHttpOverrides extends HttpOverrides{
  @override
  HttpClient createHttpClient(SecurityContext context){
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port)=> true;
  }
}

void main() {
  HttpOverrides.global = new MyHttpOverrides();

  //_loadToken();
  runApp(
    Phoenix(
        child: MyAppG()
    )
  );
}


class MyAppG extends StatelessWidget {


  build(context) {
    //metodo che cerca flutter per portare l'oggetto di quella classe sullo schermo (widget)
    return MaterialApp(//material app e' il core widget
      theme: ThemeData(
        primaryColor: Colors.white,
        fontFamily: 'Open Sans',
        buttonColor: Color(0xff00ccff),
      ),
      home: new HomeScreen()
    );
  }
}
